package com.example.lynn.pushbuttons;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.support.v4.content.ContextCompat;
import android.widget.LinearLayout;

import static com.example.lynn.pushbuttons.MainActivity.*;

/**
 * Created by lynn on 2/20/2017.
 */

public class MyView extends LinearLayout {

    public MyView(Context context) {
        super(context);

        Drawable[] images = {ContextCompat.getDrawable(context,R.drawable.cat),
                             ContextCompat.getDrawable(context,R.drawable.cow),
                             ContextCompat.getDrawable(context,R.drawable.dog),
                             ContextCompat.getDrawable(context,R.drawable.duck),
                             ContextCompat.getDrawable(context,R.drawable.monkey),
                             ContextCompat.getDrawable(context,R.drawable.pig),
                             ContextCompat.getDrawable(context,R.drawable.rooster),
                             ContextCompat.getDrawable(context,R.drawable.sheep)};

        int[] sounds = {R.raw.cat,
                        R.raw.cow,
                        R.raw.dogbark1,
                        R.raw.duck,
                        R.raw.monkey,
                        R.raw.pig,
                        R.raw.rooster,
                        R.raw.baasheep1};
    }

}

